package com.ylcd.project.presentation.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.ylcd.project.R
import com.ylcd.project.contract.UserRentalService
import com.ylcd.project.databinding.FragmentLoginBinding
import com.ylcd.project.models.ResponseLogin
import com.ylcd.project.models.UserRentalModel
import com.ylcd.project.presentation.register.RegisterFragment
import com.ylcd.project.ui.ContentActivity
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginFragment : Fragment() {
    private lateinit var etUsername: EditText
    private lateinit var etPassword: EditText
    private lateinit var btLogin: Button
    private val viewModel by viewModels<LoginViewModel>()

    private var _binding: FragmentLoginBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_product, container, false);
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        val view = binding.root
        return view;
    }
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup(view)
        setupViewModel()
        //setupLoginGoogle()
    }
    private fun getDataUser():Int{
        var id:Int=0
        val UserRentalService = UserRentalService.create().getLoginData(binding.etUsername.text.toString(),binding.etPassword.text.toString())
        UserRentalService.enqueue( object : Callback<List<UserRentalModel>> {
            override fun onResponse(call: Call<List<UserRentalModel>>?, response: Response<List<UserRentalModel>>?) {
                Log.v("responseD",response?.body().toString())
                Log.v("responseMeD",response?.message().toString())
                Log.v("responseId",response?.body()?.get(0)?.id.toString())
                val bundle = Bundle()
                id= response?.body()?.get(0)?.id as Int
                bundle.putInt("userID", id)

            }

            override fun onFailure(call: Call<List<UserRentalModel>>, t: Throwable) {
                t?.message?.let { it1 -> Log.v("responseErrorD", it1) }
            }
        })


         return id
    }
    private fun setup(view: View) {
        etUsername = view.findViewById(R.id.etUsername)
        etPassword = view.findViewById(R.id.etPassword)
        btLogin = view.findViewById(R.id.btLogin)
        binding.btnRegister.setOnClickListener{
            val bundle = Bundle()
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment, bundle)

        }
        btLogin.setOnClickListener {
            val paramObject = JSONObject()
            paramObject.put("username", etUsername.text.toString())
            paramObject.put("password",etPassword.text.toString())
            var result:String="";
            val UserRentalService = UserRentalService.create().getLogin(paramObject.get("username").toString(),paramObject.get("password").toString())
            UserRentalService.enqueue( object : Callback<ResponseLogin> {
                override fun onResponse(call: Call<ResponseLogin>?, response: Response<ResponseLogin>?) {
                    Log.v("response",response?.body()?.result.toString())
                    Log.v("responseMe",response?.message().toString())
                             result=response?.body()?.result.toString()
                    viewModel.login(etUsername.text.toString(), etPassword.text.toString(),result)
                    }

                override fun onFailure(call: Call<ResponseLogin>, t: Throwable) {
                    t?.message?.let { it1 -> Log.v("responseError", it1) }
                }
            })
            getDataUser()





        }
    }


    private fun setupViewModel() {
        viewModel.loginResultLD.observe(viewLifecycleOwner) { success ->
            if (success) {
                val bundle = Bundle()
                bundle.putInt("userID",getDataUser())
                val intent= Intent(context, ContentActivity::class.java)
                startActivity(intent)
               // findNavController().navigate(R.id.action_loginFragment_to_productFragment, bundle)
            } else {
                view?.also {
                    val snack = Snackbar.make(
                        it,
                        "El usuario o contraseña no es correcto",
                        Snackbar.LENGTH_LONG
                    )
                    snack.show()
                }
            }
        }
    }
}