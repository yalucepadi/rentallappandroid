package com.ylcd.project.presentation.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ylcd.project.models.ResponseLogin
import com.ylcd.project.models.UserRentalModel
import com.ylcd.project.repository.Repository
import kotlinx.coroutines.launch
import retrofit2.Response

class LoginViewModelWF(private val repository: Repository): ViewModel() {
    var myResponseVerificadorLogin: MutableLiveData<Response<ResponseLogin>> = MutableLiveData()
    var myResponse: MutableLiveData<Response<List<UserRentalModel>>> = MutableLiveData()

    fun getLoginResponse(username: String,password: String){
        viewModelScope.launch {
            val response=repository.getLoginResponse(username,password)
             myResponseVerificadorLogin.value= response
        }
    }
    fun getLoginDataResponse(username: String,password: String){
        viewModelScope.launch {
          val response = repository.getLoginDataResponse(username,password)
          myResponse.value=response
        }

    }

}