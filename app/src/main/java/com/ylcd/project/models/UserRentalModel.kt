package com.ylcd.project.models

import java.time.format.DateTimeFormatter

data class UserRentalModel (val id:Int,
                            val username:String,
                            val password:String,
                            val district:String,
                            val abbreviations:String,
                            val address:String,
                          )

/*val created_at: DateTimeFormatter,
val updated_at:DateTimeFormatter*/