package com.ylcd.project.repository

import com.ylcd.project.contract.RetrofitInstance
import com.ylcd.project.models.ResponseLogin
import com.ylcd.project.models.UserRentalModel
import retrofit2.Response

class Repository     {

    suspend fun getLoginResponse(username: String,password: String): Response<ResponseLogin> {
        return RetrofitInstance.api.getLoginResponse(username,password)
    }
    suspend fun getLoginDataResponse(username: String,password: String): Response<List<UserRentalModel>> {
        return RetrofitInstance.api.getLoginDataResponse(username,password)
    }
}