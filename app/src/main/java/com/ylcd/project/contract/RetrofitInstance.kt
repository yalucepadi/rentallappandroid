package com.ylcd.project.contract

import com.google.gson.GsonBuilder
import com.ylcd.project.utils.Constants.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    private val retrofit by lazy {
        val gson = GsonBuilder()
            .setLenient()
            .create()

         Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(UserRentalService.BASE_URL)
            .build()
    }

    val api: UserRentalService by lazy {
        retrofit.create(UserRentalService::class.java)
    }
}