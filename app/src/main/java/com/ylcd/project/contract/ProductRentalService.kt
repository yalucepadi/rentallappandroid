package com.ylcd.project.contract

import com.google.gson.GsonBuilder
import com.ylcd.project.models.ProductModel
import com.ylcd.project.models.UserRentalModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ProductRentalService {
    /**
     * Retorna un Product mediante el id
     */
    @GET("/product/{id}")
    fun getProduct(@Path("id")  id:Int): Call<ProductModel>

    @GET
    fun getProducts(): Call<ProductModel>
    @POST("/product/crear?userRent_id={userRent_id}&name_product={name_product}&quantity={quantity}")
    fun postProduct(   @Path("userRent_id")  userRent_id:Int,
                       @Path("name_product")  name_product:String,
                       @Path("quantity")  district:Int
    ): Call<ProductModel>
    @PUT("/product/actualizar/{id}?userRent_id={userRent_id}&name_product={name_product}&quantity={quantity}")
    fun putProduct( @Path("id")  id:Int,
                    @Path("userRent_id")  userRent_id:Int,
                    @Path("name_product")  name_product:String,
                    @Path("quantity")  district:Int
    ): Call<ProductModel>
    @DELETE("/product/eliminar/{id}")
    fun deleteProduct(@Path("id")  id:Int): Call<UserRentalModel>
    companion object {

        var BASE_URL = "http://127.0.0.1:8000/api"

        fun create() : UserRentalService {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build()

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(UserRentalService.BASE_URL)
                .build()
            return retrofit.create(UserRentalService::class.java)

        }
    }
}