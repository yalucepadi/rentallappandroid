package com.ylcd.project.models

import java.time.format.DateTimeFormatter

data class ProductModel(val id:Int,
                        val userRent_id:Int,
                        val name_product:String,
                        val quantity:Int,
                        val created_at:DateTimeFormatter,
                        val updated_at:DateTimeFormatter)
