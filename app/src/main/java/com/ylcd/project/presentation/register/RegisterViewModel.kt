package com.ylcd.project.presentation.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ylcd.project.common.SingleLiveEvent

class RegisterViewModel : ViewModel() {
    private val _registerResultSLE = SingleLiveEvent<Boolean>()
    val registerResultLD: LiveData<Boolean> = _registerResultSLE

    fun register(username: String, password: String,district:String,abbreviations:String,address:String,result:String) {
            if(result=="Created") {
                _registerResultSLE.value = true
            }
        else
        { _registerResultSLE.value = false}


               }


    }
