package com.ylcd.project.presentation.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ylcd.project.common.SingleLiveEvent
import com.ylcd.project.models.UserRental
import com.ylcd.project.models.UserRentalRepository

internal class ProductViewModel: ViewModel() {
    private val _productResultSLE = SingleLiveEvent<Boolean>()
    val productResultLD: LiveData<Boolean> = _productResultSLE
    private fun loadProduct(result: String) {
        if(result==""){
            _productResultSLE.value = true

        }
        else{
            if(result==""){

                _productResultSLE.value = false}

        }
    }

}