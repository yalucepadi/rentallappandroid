package com.ylcd.project.contract

import com.google.gson.GsonBuilder
import com.ylcd.project.models.ResponseLogin
import com.ylcd.project.models.UserRentalModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


interface UserRentalService {
    /**
     * Retorna un userRental mediante el id
     */

    @GET("login/{username}/{password}")
    fun getLogin(
        @Path("username") userName: String,
        @Path("password") password: String,
    ):Call<ResponseLogin>
    @GET("login/{username}/{password}")
    fun getLoginResponse(
        @Path("username") userName: String,
        @Path("password") password: String,
    ): Response<ResponseLogin>

    @GET("dataUser/{username}/{password}")
    fun getLoginData(
        @Path("username") userName: String,
        @Path("password") password: String,
    ):Call<List<UserRentalModel>>
    @GET("dataUser/{username}/{password}")
    fun getLoginDataResponse(
        @Path("username") userName: String,
        @Path("password") password: String,
    ):Response<List<UserRentalModel>>

    @GET("/{id}")
    fun getUserRental(@Path("id")  id:Int):Call<UserRentalModel>

    @GET
    fun getUserRentals():Call<UserRentalModel>
    @POST("crear")
    //crear?username={userName}&password={password}&district={district}&abbreviations={abbreviations}&address={address}
    fun postUserRental(@Query("username")  userName:String,
                       @Query("password")  password:String,
                       @Query("district")  district:String,
                       @Query("abbreviations")  abbreviations:String,
                       @Query("address")  address:String
    ):Call<UserRentalModel>
    @PUT("/actualizar/{id}?username={userName}&password={password}&district={district}&abbreviations={abbreviations}&address={address}")
    fun putUserRental(@Path("id")  id:Int,
                      @Path("userName")  userName:String,
                       @Path("password")  password:String,
                       @Path("district")  district:String,
                       @Path("abbreviations")  abbreviations:String,
                       @Path("address")  address:String
    ):Call<UserRentalModel>
    @DELETE("/eliminar/{id}")
    fun deleteUserRental(@Path("id")  id:Int):Call<UserRentalModel>
    companion object {

        var BASE_URL = "http://10.0.2.2:8000/api/userRent/"

        fun create() : UserRentalService {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build()

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(UserRentalService::class.java)

        }
}}