package com.ylcd.project.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ylcd.project.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}