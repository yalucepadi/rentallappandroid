package com.ylcd.project.presentation.register

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.ylcd.project.R
import com.ylcd.project.contract.UserRentalService
import com.ylcd.project.databinding.RegisterFragmentBinding
import com.ylcd.project.models.ResponseLogin
import com.ylcd.project.models.UserRentalModel

import com.ylcd.project.presentation.login.LoginViewModel
import com.ylcd.project.ui.ContentActivity
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterFragment : Fragment() {

    private val viewModel by viewModels<RegisterViewModel>()

    private var _binding: RegisterFragmentBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.register_fragment, container, false);
        _binding = RegisterFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view;
    }
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       setup(view)
        setupViewModel()

    }
    private fun setup(view: View) {
        binding.btnRegresar.setOnClickListener {
            val bundle = Bundle()
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment, bundle)
        }
        binding.btnRegistrar.setOnClickListener{
            var result:String="";
            val UserRentalService = UserRentalService.create().postUserRental(
                binding.txtNombre.text.toString(),
                binding.txtPassword.text.toString(),
                binding.txtDistrict.text.toString(),
                binding.txtAbbreviations.text.toString(),
                binding.txtAddress.text.toString())
            UserRentalService.enqueue( object : Callback<UserRentalModel> {
                override fun onResponse(call: Call<UserRentalModel>?, response: Response<UserRentalModel>?) {
                    Log.v("responseM",response?.message().toString())
                    Log.v("responseRb",response?.body().toString())
                    Log.v("responseRaw",response?.raw().toString())
                    Log.v("responseRb",response?.body().toString())
                    result=response?.message().toString()
                    viewModel.register(binding.txtNombre.text.toString(),
                        binding.txtPassword.text.toString(),
                        binding.txtDistrict.text.toString(),
                        binding.txtAbbreviations.text.toString(),
                        binding.txtAddress.text.toString(),result)
                }

                override fun onFailure(call: Call<UserRentalModel>, t: Throwable) {
                    t?.message?.let { it1 -> Log.v("responseError", it1) }
                }
            })
        }


    }
    private fun setupViewModel() {
        viewModel.registerResultLD.observe(viewLifecycleOwner) { success ->
            if (success) {
                val bundle = Bundle()
                //bundle.putInt("userID", 1)
                Toast.makeText(context,"Registo correcto, regrese al login para iniciar",Toast.LENGTH_LONG).show()
                view?.also {
                    val snack = Snackbar.make(
                        it,
                        "Registo correcto, regrese al login para iniciar",
                        Snackbar.LENGTH_LONG
                    )
                    snack.show()}
                // findNavController().navigate(R.id.action_loginFragment_to_productFragment, bundle)
            } else {
                view?.also {
                    val snack = Snackbar.make(
                        it,
                        "fallo en registro",
                        Snackbar.LENGTH_LONG
                    )
                    snack.show()
                }
            }
        }
    }

}


