package com.ylcd.project.presentation.product

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.ylcd.project.R
import com.ylcd.project.databinding.FragmentProductBinding
import com.ylcd.project.ui.ContentActivity
import kotlinx.android.synthetic.main.fragment_product.*
import org.w3c.dom.Text


class ProductFragment : Fragment() {

    private val viewModel by viewModels<ProductViewModel>()

    private var _binding: FragmentProductBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_product, container, false);
        _binding = FragmentProductBinding.inflate(inflater, container, false)
        val view = binding.root
        return view;
    }
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup(view)
        setupViewModel()
        //setupLoginGoogle()
    }
    private fun setup(view: View) {
        val userID = arguments?.getInt("userID")
        binding.edtOwner.text= userID.toString()
    }

    private fun setupViewModel() {

        viewModel.productResultLD.observe(viewLifecycleOwner) { success ->
            if (success) {
                val bundle = Bundle()

                // findNavController().navigate(R.id.action_loginFragment_to_productFragment, bundle)
            } else {

            }
        }

            }
        }

