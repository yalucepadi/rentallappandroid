package com.ylcd.project.presentation.login

import android.app.Activity
import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.constraintlayout.motion.utils.ViewState
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ylcd.project.common.SingleLiveEvent
import com.ylcd.project.contract.UserRentalService
import com.ylcd.project.models.UserRental
import com.ylcd.project.models.UserRentalModel
import com.ylcd.project.models.UserRentalRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel : ViewModel() {
    private val _loginResultSLE = SingleLiveEvent<Boolean>()
    val loginResultLD: LiveData<Boolean> = _loginResultSLE

    fun login(username: String, password: String,result: String) {
         if(result=="login success"){
             _loginResultSLE.value = true

         }
        else{
            if(result=="bad login"){

             _loginResultSLE.value = false}

        }
    }

}