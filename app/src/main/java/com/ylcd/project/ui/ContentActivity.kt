package com.ylcd.project.ui


import android.content.Intent
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.navigation.NavigationView
import com.ylcd.project.R
import com.ylcd.project.databinding.ActivityContentBinding
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavGraphNavigator
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import com.ylcd.project.presentation.login.LoginFragment
import com.ylcd.project.presentation.product.ProductFragment
import kotlinx.android.synthetic.main.activity_content.*
import kotlin.random.Random



class ContentActivity : AppCompatActivity(){
   lateinit var  toogle : ActionBarDrawerToggle
   lateinit var  drawerLayout: DrawerLayout
    private lateinit var binding: ActivityContentBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)

        setContentView(R.layout.activity_content)
        binding = ActivityContentBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
         drawerLayout  = findViewById(R.id.drawerLayout)
        val  navView : NavigationView= findViewById(R.id.nav_view)
        toogle =  ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close)
        drawerLayout.addDrawerListener(toogle)
        toogle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        navView.setNavigationItemSelectedListener {
            it.isChecked=true
            when(it.itemId){
                R.id.nav_product-> replaceFragment(ProductFragment(),it.title.toString())
                R.id.nav_logout->  { val intent= Intent(this, MainActivity::class.java)
                startActivity(intent)}
            }
            true
        }



}

  private fun replaceFragment(fragment:Fragment,title:String){
     val fragmentManager = supportFragmentManager
     val fragmetTransaction= fragmentManager.beginTransaction()
     fragmetTransaction.replace(R.id.frameLayout,fragment)
     fragmetTransaction.commit()
      drawerLayout.closeDrawers()
     setTitle(title)
  }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(toogle.onOptionsItemSelected(item)){

           return  true
        }
        return super.onOptionsItemSelected(item)
    }
}